//
//  ViewController.m
//  View Objects In Code
//
//  Created by RJ Militante on 2/11/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UITextView *textView1;
@property (strong, nonatomic) IBOutlet UIButton *anotherButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.label1 = [[UILabel alloc] initWithFrame:CGRectMake(20, 60, 280, 22)];
    self.label1.backgroundColor = [UIColor greenColor];
    
    self.textView1 = [[UITextView alloc] initWithFrame:CGRectMake(20, 110, 280, 30)];
    self.textView1.backgroundColor = [UIColor blueColor];
    self.textView1.text = @"placeholder";
    
    /* Create a UIButton object. Use the class method bottonWithType and select a RoundedRect button */
    self.anotherButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    /* Set the button's frame property by creating a CGRect */
    self.anotherButton.frame = CGRectMake(20, 160, 280, 30);
    /* Set the button's background color to green. */
    self.anotherButton.backgroundColor = [UIColor greenColor];
    /* Set the button's title for state. We'll use the control state UIControlStateNormal. */
    [self.anotherButton setTitle:@"Press me... Please!" forState:UIControlStateNormal];
    /* Add the UIButton which is a subclass of UIView to the background view. */
    [self.view addSubview:self.anotherButton];
    
    /* Create a target action for the UIButton object we created. In this case we set the target to be self. Specifically, the UIButton's target is this instance of the CCViewController class. The method that should evaluate is named didPressButton. Notice that we use the @selector keyword to let the compiler know we'll be choosing a method name. Also notice the colon which tells the compiler that there will be an argument for this method. We choose UIControlEventTouchUpInside which has been the default control event we've choose when creating IBActions. */
    [self.anotherButton addTarget:self action:@selector(didPressButton:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:self.label1];
    [self.view addSubview:self.textView1];
}

-(void)didPressButton:(UIButton *)button
{
    NSLog(@"I'm doing everything in code now!");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
